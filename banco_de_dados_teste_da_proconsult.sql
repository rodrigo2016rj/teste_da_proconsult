-- -------------------------------------------------------------------------
-- banco_de_dados_teste_da_proconsult:

DROP SCHEMA IF EXISTS banco_de_dados_teste_da_proconsult;

CREATE SCHEMA IF NOT EXISTS banco_de_dados_teste_da_proconsult 
DEFAULT CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

USE banco_de_dados_teste_da_proconsult;

-- -------------------------------------------------------------------------
-- Tabela usuario:

DROP TABLE IF EXISTS usuario;

CREATE TABLE IF NOT EXISTS usuario (
  pk_usuario INT NOT NULL AUTO_INCREMENT,
  nome_completo VARCHAR(200) NOT NULL,
  cpf VARCHAR(14) NOT NULL,
  email VARCHAR(160) NOT NULL,
  senha VARCHAR(120) NOT NULL,
  momento_do_cadastro DATETIME NOT NULL,
  tipo ENUM('cliente', 'colaborador') NOT NULL DEFAULT 'cliente',
  PRIMARY KEY (pk_usuario),
  UNIQUE INDEX nome_completo_UNICA (nome_completo ASC),
  UNIQUE INDEX cpf_UNICA (cpf ASC),
  UNIQUE INDEX email_UNICA (email ASC))
ENGINE = InnoDB;

-- -------------------------------------------------------------------------
-- Tabela chamado:

DROP TABLE IF EXISTS chamado;

CREATE TABLE IF NOT EXISTS chamado(
  pk_chamado INT NOT NULL AUTO_INCREMENT,
  fk_usuario INT NOT NULL,
  titulo VARCHAR(60) NOT NULL,
  descricao VARCHAR(2000) NOT NULL,
  anexos VARCHAR(1000),
  status ENUM('aberto', 'em_atendimento', 'finalizado') NOT NULL DEFAULT 'aberto',
  momento_da_abertura DATETIME NOT NULL,
  momento_do_inicio_do_atendimento DATETIME,
  momento_da_finalizacao DATETIME,
  PRIMARY KEY (pk_chamado),
  INDEX fk_usuario_INDICE (fk_usuario ASC),
  CONSTRAINT fk_usuario_tabela_chamado
    FOREIGN KEY (fk_usuario)
    REFERENCES usuario (pk_usuario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;

-- -------------------------------------------------------------------------
-- Tabela resposta:

DROP TABLE IF EXISTS resposta;

CREATE TABLE IF NOT EXISTS resposta(
  pk_resposta INT NOT NULL AUTO_INCREMENT,
  fk_usuario INT NOT NULL,
  fk_chamado INT NOT NULL,
  mensagem VARCHAR(2000) NOT NULL,
  anexos VARCHAR(1000),
  momento_do_envio DATETIME NOT NULL,
  PRIMARY KEY (pk_resposta),
  INDEX fk_usuario_INDICE (fk_usuario ASC),
  INDEX fk_chamado_INDICE (fk_chamado ASC),
  CONSTRAINT fk_usuario_tabela_resposta
    FOREIGN KEY (fk_usuario)
    REFERENCES usuario (pk_usuario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_chamado_tabela_resposta
    FOREIGN KEY (fk_chamado)
    REFERENCES chamado (pk_chamado)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;

-- -------------------------------------------------------------------------
-- Dados de exemplo:

-- Usuário colaborador:
INSERT INTO usuario (nome_completo, cpf, email, senha, momento_do_cadastro, tipo) VALUES ('Rodrigo Diniz da Silva Fictício', '999.999.999-01', 'exemplo@emailfalso.rds', '$2y$10$hveYLOB.2WhUI026E2c/oen75qG3GJi3OpWgImW.2FEARDEKZ6ZrW', '2023-09-16 19:01:00', 'colaborador');
