## Sobre
<p>Este sistema foi feito com PHP 7.3.9, Laravel 8, Vue 3, Inertia, Axios, CSS 3, HTML 5 e MySQL Server 5.7.38.</p>

<br/>

## Motivação
<p>O que me motivou a fazer este sistema foi:<br/>
Tentar ser aprovado em um processo seletivo de emprego.</p>

<br/>

## Instruções
<p>Para ver o resultado em um ambiente de desenvolvimento siga as instruções:</p>

<p>Inicie o MySQL Server.</p>

<p>Utilize o banco de dados contido no arquivo banco_de_dados_teste_da_proconsult.sql.</p>

<p>Configure o MySQL Server para que o banco de dados deste sistema seja acessado por username root sem senha pela porta 3305.</p>

<p>Se você preferir, você pode configurar neste sistema um outro username, uma outra senha e uma outra porta.</p>

<p>Configure seu PHP pelo arquivo php.ini e certifique-se de deixar ativado intl e mbstring.</p>

<p>Coloque o diretório teste_da_proconsult dentro do endereço DocumentRoot do seu Servidor Apache. Exemplo: coloque dentro de htdocs do XAMPP. Geralmente o DocumentRoot é o diretório htdocs do XAMPP e você pode consultar ou mudar o endereço de DocumentRoot pelo arquivo de configuração do Servidor Apache (exemplo: arquivo httpd.conf).</p>

<p>Configure um VirtualHost no Servidor Apache para este sistema.<br/>
Dica: configure com a porta 80 e ServerName localhost, se tiver dúvida procure algum tutorial.<br/>
Se utiliza XAMPP, o arquivo de configuração do Servidor Apache para VirtualHost será apache\conf\extra\httpd-vhosts.conf<br/>
Exemplo de VirtualHost configurado:<br/>
<code>&lt;VirtualHost *:80&gt;</code><br/>
<code>&nbsp;&nbsp;DocumentRoot "C:\Users\Rodrigo\Servidores\XAMPP001\htdocs\teste_da_proconsult\public"</code><br/>
<code>&nbsp;&nbsp;ServerName localhost</code><br/>
<code>&lt;/VirtualHost&gt;</code></p>

<p>Sugestão: Configure também SSL (Secure Sockets Layer) ou TLS (Transport Layer Security) no seu Servidor Apache para este sistema, pois este sistema possui campos de senha e é importante verificar em desenvolvimento como o sistema ficará ao ser acessado com HTTPS nos navegadores.</p>

<p>Inicie ou reinicie o Servidor Apache.</p>

<p>Dentro do diretório teste_da_proconsult execute os comandos:<br/>
composer install<br/>
npm install</p>

<p>Renomeie o arquivo .env.example para .env<br/>
Gere a chave APP_KEY do arquivo .env pelo comando: php artisan key:generate</p>

<p>Ainda dentro do diretório teste_da_proconsult execute o comando:<br/>
npm run dev</p>

<p>Acesse o endereço http://localhost:80 em um navegador. Se tiver configurado SSL ou TLS, acesse o endereço https://localhost:80 em um navegador.</p>

<br/>

## Outras informações
<p>Coloquei no banco de dados deste sistema um usuário do tipo colaborador para ser utilizado de exemplo.<br/>
Para fazer login no sistema com o usuário de exemplo utilize:<br/>
E-mail: exemplo@emailfalso.rds<br/>
Senha: senha</p>

<br/>
