<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaginaInicialController;
use App\Http\Controllers\CadastreSeController;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\ChamadosController;
use App\Http\Controllers\ChamadoController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/* Página Padrão */
Route::get('/', [PaginaInicialController::class, 'carregar_pagina']);

/* Página Inicial */
Route::get('/pagina_inicial', [PaginaInicialController::class, 'carregar_pagina']);
Route::post('/pagina_inicial/entrar', [PaginaInicialController::class, 'entrar']);
Route::get('/pagina_inicial/sair', [PaginaInicialController::class, 'sair']);

/* Cadastre-se */
Route::get('/cadastre_se', [CadastreSeController::class, 'carregar_pagina']);
Route::post('/cadastre_se/entrar', [CadastreSeController::class, 'entrar']);
Route::get('/cadastre_se/sair', [CadastreSeController::class, 'sair']);
Route::post('/cadastre_se/cadastrar', [CadastreSeController::class, 'cadastrar']);

/* Usuários */
Route::get('/usuarios', [UsuariosController::class, 'carregar_pagina']);
Route::post('/usuarios/entrar', [UsuariosController::class, 'entrar']);
Route::get('/usuarios/sair', [UsuariosController::class, 'sair']);
Route::get('/usuarios/mudar_tipo', [UsuariosController::class, 'mudar_tipo']);

/* Chamados */
Route::get('/chamados', [ChamadosController::class, 'carregar_pagina']);
Route::post('/chamados/entrar', [ChamadosController::class, 'entrar']);
Route::get('/chamados/sair', [ChamadosController::class, 'sair']);
Route::post('/chamados/abrir_novo_chamado', [ChamadosController::class, 'abrir_novo_chamado']);

/* Chamado */
Route::get('/chamado', [ChamadoController::class, 'carregar_pagina']);
Route::post('/chamado/entrar', [ChamadoController::class, 'entrar']);
Route::get('/chamado/sair', [ChamadoController::class, 'sair']);
Route::post('/chamado/responder_via_ajax', [ChamadoController::class, 'responder_via_ajax']);
Route::post('/chamado/finalizar_chamado_via_ajax', [ChamadoController::class, 'finalizar_chamado_via_ajax']);
