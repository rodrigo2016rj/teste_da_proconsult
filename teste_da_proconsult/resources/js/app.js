require("./bootstrap");

import {createApp, h} from "vue";
import {createInertiaApp} from "@inertiajs/inertia-vue3";

/* Coloque uma tag title no seu inertia.blade.php */
const titulo_do_sistema = document.getElementsByTagName("title")[0].innerText;

createInertiaApp({
  title: (title) => `${titulo_do_sistema} - ${title}`, //Título do sistema mais título da página
  resolve: (name) => require(`./Pages/${name}.vue`),
  setup({el, app, props, plugin}){
    el.id = "div_app_template";
    delete el.dataset.page;
    return createApp({render: () => h(app, props)}).use(plugin).mount(el);
  }
});
