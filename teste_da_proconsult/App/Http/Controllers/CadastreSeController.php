<?php

namespace App\Http\Controllers;

use App\Models\CadastreSeModel;
use App\Models\Entidades\Usuario;
use DateTime;
use DateTimeZone;
use Inertia\Inertia;

final class CadastreSeController extends TemplateLayoutController{

  public function carregar_pagina($redirecionar = false){
    if($redirecionar){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header('Location: /cadastre_se');
      die;
    }

    $this->conferir_se_o_usuario_esta_logado();
    $valores = $this->get_valores();
    $sessao = session();

    /* Especificando a página do sistema para os links e outras tags */
    $valores['template_layout']['pagina_do_sistema'] = 'cadastre_se';

    /* Mostrando mensagem caso exista alguma */
    if($sessao->has('mensagem_do_sistema')){
      $valores['template_layout']['mensagem'] = $sessao->get('mensagem_do_sistema');
      $sessao->forget('mensagem_do_sistema');
      $sessao->save();
    }

    /* Variável que guarda a mensagem da página começa inicialmente vazia */
    $mensagem = '';

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['cadastre_se']['nome_completo'] = '';
    $valores['cadastre_se']['cpf'] = '';
    $valores['cadastre_se']['email'] = '';

    /* Recolocando valores preenchidos previamente pelo usuário no formulário */
    if($sessao->has('backup_do_formulario_da_pagina_cadastre_se')){
      $backup = $sessao->get('backup_do_formulario_da_pagina_cadastre_se');
      $valores['cadastre_se']['nome_completo'] = $backup['nome_completo'];
      $valores['cadastre_se']['cpf'] = $backup['cpf'];
      $valores['cadastre_se']['email'] = $backup['email'];
      $sessao->forget('backup_do_formulario_da_pagina_cadastre_se');
      $sessao->save();
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_cadastre_se')){
      $mensagem = $sessao->get('mensagem_da_pagina_cadastre_se');
      $sessao->forget('mensagem_da_pagina_cadastre_se');
      $sessao->save();
    }

    $valores['cadastre_se']['mensagem_da_pagina'] = $mensagem;
    return Inertia::render('cadastre_se/cadastre_se', $valores);
  }

  public function entrar(){
    $this->entrar_padronizado();
    $this->carregar_pagina(true);
    die;
  }

  public function sair(){
    $this->sair_padronizado();
    $this->carregar_pagina(true);
    die;
  }

  public function cadastrar(){
    $sessao = session();
    $cadastre_se_model = new CadastreSeModel();
    $usuario = new Usuario();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $nome_completo = trim($requisicao->post('nome_completo') ?? '');
    $cpf = trim($requisicao->post('cpf') ?? '');
    $email = trim($requisicao->post('email') ?? '');
    $senha = $requisicao->post('senha');
    $senha_novamente = $requisicao->post('senha_novamente');

    /* Removendo espaços vazios do e-mail digitado caso existam */
    $email = str_replace(' ', '', $email);

    /* Fazendo backup do formulário */
    $backup_do_formulario['nome_completo'] = $nome_completo;
    $backup_do_formulario['cpf'] = $cpf;
    $backup_do_formulario['email'] = $email;
    $sessao->put('backup_do_formulario_da_pagina_cadastre_se', $backup_do_formulario);
    $sessao->save();

    /* Validando o nome completo */
    if($nome_completo === ''){
      $mensagem = 'O campo nome precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $minimo = $usuario->quantidade_minima_de_caracteres('nome_completo');
    $maximo = $usuario->quantidade_maxima_de_caracteres('nome_completo');
    $quantidade = mb_strlen($nome_completo);
    if($quantidade < $minimo){
      $mensagem = "O campo nome precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = "O campo nome não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $array_resultado = $cadastre_se_model->verificar_disponibilidade_de_nome_completo($nome_completo);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $usuario->set_nome_completo($nome_completo);

    /* Validando o CPF */
    if($cpf === ''){
      $mensagem = 'O campo CPF precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $array_resultado = $cadastre_se_model->verificar_disponibilidade_de_cpf($cpf);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $padrao = $usuario->padrao_para_cpf();
    if(!preg_match($padrao, $cpf)){
      $mensagem = 'O formato do CPF não é válido.';
      $mensagem .= ' O CPF precisa ser preenchido no formato correto.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $usuario->set_cpf($cpf);

    /* Validando o e-mail */
    if($email === ''){
      $mensagem = 'O campo e-mail precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $quantidade_de_arrobas = substr_count($email, '@');
    if($quantidade_de_arrobas > 1){
      $mensagem = 'O campo e-mail precisa ter somente um caractere @.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade_de_arrobas < 1){
      $mensagem = 'O campo e-mail precisa ter pelo menos um caractere @.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if(str_starts_with($email, '@')){
      $mensagem = 'A parte antes do @ no campo e-mail precisa ser preenchida.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $parte_do_arroba_e_apos = strstr($email, '@');
    if($parte_do_arroba_e_apos === '@'){
      $mensagem = 'A parte após o @ no campo e-mail, domínio do e-mail, precisa ser preenchida.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if(strpos($parte_do_arroba_e_apos, '.') === false){
      $mensagem = 'A parte após o @ no campo e-mail, domínio do e-mail, não foi preenchida';
      $mensagem .= ' corretamente. Está faltando o caractere . (ponto).';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if(strpos($parte_do_arroba_e_apos, '.') === 1){
      $mensagem = 'A parte após o @ no campo e-mail, domínio do e-mail, não foi preenchida';
      $mensagem .= ' corretamente. Está faltando a parte antes do ponto.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $parte_do_ponto_e_apos = strstr($parte_do_arroba_e_apos, '.');
    if($parte_do_ponto_e_apos === '.'){
      $mensagem = 'A parte após o @ no campo e-mail, domínio do e-mail, não foi preenchida';
      $mensagem .= ' corretamente. Está faltando a parte após o ponto.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $array_resultado = $cadastre_se_model->verificar_disponibilidade_de_email($email);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $minimo = $usuario->quantidade_minima_de_caracteres('email');
    $maximo = $usuario->quantidade_maxima_de_caracteres('email');
    $quantidade = mb_strlen($email);
    if($quantidade < $minimo){
      $mensagem = "O campo e-mail precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = "O campo e-mail não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $usuario->set_email($email);

    /* Validando a senha */
    if($senha === null or $senha === ''){
      $mensagem = 'O campo senha precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $minimo = $usuario->quantidade_minima_de_caracteres('senha');
    $maximo = $usuario->quantidade_maxima_de_caracteres('senha');
    $quantidade = mb_strlen($senha);
    if($quantidade < $minimo){
      $mensagem = "O campo senha precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = "O campo senha não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    if($senha_novamente === null or $senha_novamente === ''){
      $mensagem = 'O segundo campo de senha precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    if($senha_novamente !== $senha){
      $mensagem = 'O valor do segundo campo de senha precisa ser o mesmo valor do primeiro campo';
      $mensagem .= ' de senha. Essa validação existe para te ajudar a não preencher errado.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    /* Criptografando a senha */
    $senha = $this->criptografar_senha_do_usuario($senha);
    $usuario->set_senha($senha);

    /* Momento atual sem fuso horário, pois no banco de dados armazeno sem fuso horário (timezone) */
    $sem_fuso_horario = new DateTimeZone('GMT');
    $objeto_date_time = new DateTime('now', $sem_fuso_horario);
    $momento_atual = $objeto_date_time->format('Y-m-d H:i:s');
    $usuario->set_momento_do_cadastro($momento_atual);

    /* Cadastrando o usuário no banco de dados */
    $array_resultado = $cadastre_se_model->cadastrar_usuario($usuario);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }else{
      $mensagem = 'Seu cadastro foi realizado com sucesso.';
      $sessao->put('mensagem_da_pagina_cadastre_se', $mensagem);
      $sessao->forget('backup_do_formulario_da_pagina_cadastre_se');
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
  }

}
