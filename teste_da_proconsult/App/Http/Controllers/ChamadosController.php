<?php

namespace App\Http\Controllers;

use App\Models\ChamadosModel;
use App\Models\Entidades\Chamado;
use Inertia\Inertia;
use DateTime;
use DateTimeZone;

final class ChamadosController extends TemplateLayoutController{
  private const QUANTIDADE_PADRAO_POR_PAGINA = 20;

  public function carregar_pagina($redirecionar = false){
    if($redirecionar){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header('Location: /chamados');
      die;
    }

    $this->conferir_se_o_usuario_esta_logado();
    $valores = $this->get_valores();
    $sessao = session();

    /* Especificando a página do sistema para os links e outras tags */
    $valores['template_layout']['pagina_do_sistema'] = 'chamados';

    /* Mostrando mensagem caso exista alguma */
    if($sessao->has('mensagem_do_sistema')){
      $valores['template_layout']['mensagem'] = $sessao->get('mensagem_do_sistema');
      $sessao->forget('mensagem_do_sistema');
      $sessao->save();
    }

    /* Variável que guarda a mensagem da página começa inicialmente vazia */
    $mensagem = '';

    /* Variável que guarda a mensagem do formulário novo chamado começa inicialmente vazia */
    $mensagem_do_formulario_novo_chamado = '';

    /* $mostrar_formulario_de_abrir_novo_chamado a princípio é true */
    $mostrar_formulario_de_abrir_novo_chamado = true;

    /* Carregando lista de status */
    $chamado = new Chamado();
    $valores['chamados']['lista_de_status_do_chamado'] = $chamado->enum_status();

    /* Carregando lista de quantidades por página */
    $quantidades_por_pagina = $this->criar_array_quantidades_por_pagina();
    $valores['chamados']['quantidades_por_pagina'] = $quantidades_por_pagina;

    /* Carregando lista de chamados */
    $valores['chamados']['lista_de_chamados'] = $this->mostrar_chamados();

    /* Recolocando valores preenchidos previamente pelo usuário no formulário */
    if($sessao->has('backup_do_formulario_da_pagina_chamados')){
      $backup = $sessao->get('backup_do_formulario_da_pagina_chamados');
      $valores['chamados']['titulo_do_chamado'] = $backup['titulo_do_chamado'];
      $valores['chamados']['descricao_do_chamado'] = $backup['descricao_do_chamado'];
      $valores['chamados']['anexos_do_chamado'] = $backup['anexos_do_chamado'];
      $sessao->forget('backup_do_formulario_da_pagina_chamados');
      $sessao->save();
    }

    /* Verificando se o usuário está logado */
    $usuario_logado = $this->get_usuario_logado();
    if($usuario_logado === null){
      $mensagem = 'Você precisa entrar para poder acessar a página de chamados.';
      unset($valores['chamados']['lista_de_status_do_chamado']);
      unset($valores['chamados']['quantidades_por_pagina']);
      unset($valores['chamados']['lista_de_chamados']);
      unset($valores['chamados']['titulo_do_chamado']);
      unset($valores['chamados']['descricao_do_chamado']);
      unset($valores['chamados']['anexos_do_chamado']);
      $mostrar_formulario_de_abrir_novo_chamado = false;
    }elseif($usuario_logado->get_tipo() !== 'cliente'){
      $mostrar_formulario_de_abrir_novo_chamado = false;
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_chamados')){
      $mensagem = $sessao->get('mensagem_da_pagina_chamados');
      $sessao->forget('mensagem_da_pagina_chamados');
      $sessao->save();
    }
    if($sessao->has('mensagem_do_formulario_novo_chamado_da_pagina_chamados')){
      $mensagem_do_formulario_novo_chamado = $sessao->get('mensagem_do_formulario_novo_chamado_da_pagina_chamados');
      $sessao->forget('mensagem_do_formulario_novo_chamado_da_pagina_chamados');
      $sessao->save();
    }

    $valores['chamados']['mostrar_formulario_de_abrir_novo_chamado'] = $mostrar_formulario_de_abrir_novo_chamado;
    $valores['chamados']['mensagem_da_pagina'] = $mensagem;
    $valores['chamados']['mensagem_do_formulario_novo_chamado'] = $mensagem_do_formulario_novo_chamado;
    return Inertia::render('chamados/chamados', $valores);
  }

  public function entrar(){
    $this->entrar_padronizado();
    $this->carregar_pagina(true);
    die;
  }

  public function sair(){
    $this->sair_padronizado();
    $this->carregar_pagina(true);
    die;
  }

  private function criar_array_quantidades_por_pagina(){
    $quantidades_por_pagina['5'] = 5;
    $quantidades_por_pagina['10'] = 10;
    $quantidades_por_pagina['15'] = 15;
    $quantidades_por_pagina['20'] = 20;
    $quantidades_por_pagina['25'] = 25;
    $quantidades_por_pagina['30'] = 30;
    $quantidades_por_pagina['40'] = 40;
    $quantidades_por_pagina['50'] = 50;
    $quantidades_por_pagina['60'] = 60;
    $quantidades_por_pagina['100'] = 100;
    $quantidades_por_pagina['120'] = 120;

    return $quantidades_por_pagina;
  }

  private function mostrar_chamados(){
    $chamados_model = new ChamadosModel();
    $chamado = new Chamado();

    $requisicao = $this->get_requisicao();
    $valores = array();

    /* Preparando os filtros */
    $filtros = array();
    $filtro_nome_do_usuario = trim($requisicao->get('filtro_nome_do_usuario') ?? '');
    if($filtro_nome_do_usuario !== ''){
      $filtros['nome_do_usuario'] = $filtro_nome_do_usuario;
    }
    $valores['filtro_nome_do_usuario'] = $filtro_nome_do_usuario;
    $valores['mostrar_filtro_nome_do_usuario'] = true;

    $filtro_titulo_do_chamado = trim($requisicao->get('filtro_titulo_do_chamado') ?? '');
    if($filtro_titulo_do_chamado !== ''){
      $filtros['titulo_do_chamado'] = $filtro_titulo_do_chamado;
    }
    $valores['filtro_titulo_do_chamado'] = $filtro_titulo_do_chamado;

    $filtro_status_do_chamado = trim($requisicao->get('filtro_status_do_chamado') ?? '');
    $array_lista_de_status = $chamado->enum_status();
    if(isset($array_lista_de_status[$filtro_status_do_chamado])){
      $filtros['status_do_chamado'] = $filtro_status_do_chamado;
    }else{
      $filtro_status_do_chamado = '';
    }
    $valores['filtro_status_do_chamado'] = $filtro_status_do_chamado;

    /* Filtro CPF do usuário serve para que o cliente não veja os chamados de outras pessoas */
    $usuario_logado = $this->get_usuario_logado();
    if($usuario_logado !== null and $usuario_logado->get_tipo() === 'cliente'){
      $filtros['cpf_do_usuario'] = $usuario_logado->get_cpf();

      //Filtro nome do usuário se torna desnecessário nesse caso
      unset($filtros['nome_do_usuario']);
      $valores['mostrar_filtro_nome_do_usuario'] = false;
    }

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    switch($ordenacao){
      case 'padrao':
      case 'momento_da_abertura_em_ordem_cronologica':
      case 'momento_da_abertura_em_ordem_cronologica_inversa':
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = (int) $requisicao->get('quantidade_por_pagina');
    $quantidades_por_pagina = $this->criar_array_quantidades_por_pagina();
    if(in_array($quantidade_por_pagina, $quantidades_por_pagina)){
      $valores['quantidade_por_pagina'] = $quantidade_por_pagina;
    }else{
      $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;
      $valores['quantidade_por_pagina'] = 'padrao';
    }

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_da_tabela_de_chamados($filtros,
      $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores['pagina_atual'] = $pagina;
    $valores['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = max($descartar, 0);

    /* Preparando o resultado */
    $chamados = $chamados_model->selecionar_chamados($filtros, $ordenacao, $quantidade_por_pagina,
      $descartar);
    $array_chamados = array();
    foreach($chamados as $chamado){
      $array_chamado = array();

      $array_chamado['id_do_chamado'] = $chamado->get_pk_chamado();
      $array_chamado['titulo'] = $chamado->get_titulo();
      $array_chamado['status'] = $chamado->enum_status()[$chamado->get_status()];

      /* Informações do cliente que abriu o chamado */
      $array_chamado['nome_completo_do_cliente'] = $chamado->get_usuario()->get_nome_completo();
      $array_chamado['email_do_cliente'] = $chamado->get_usuario()->get_email();

      /* Colocando os momentos no horário de Brasília */
      $sem_fuso_horario = new DateTimeZone('GMT');
      $fuso_horario_de_brasilia = new DateTimeZone('-0300');

      $momento_da_abertura = $chamado->get_momento_da_abertura();
      $objeto_date_time = new DateTime($momento_da_abertura, $sem_fuso_horario);
      $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);
      $momento_da_abertura = $objeto_date_time->format('Y-m-d H:i:s');
      $momento_da_abertura = $this->converter_para_horario_data_do_html($momento_da_abertura);
      $array_chamado['momento_da_abertura'] = $momento_da_abertura;

      $momento_do_inicio_do_atendimento = $chamado->get_momento_do_inicio_do_atendimento();
      $objeto_date_time = new DateTime($momento_do_inicio_do_atendimento, $sem_fuso_horario);
      $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);
      $momento_do_inicio_do_atendimento = $objeto_date_time->format('Y-m-d H:i:s');
      $momento_do_inicio_do_atendimento = $this->converter_para_horario_data_do_html($momento_do_inicio_do_atendimento);
      $array_chamado['momento_do_inicio_do_atendimento'] = $momento_do_inicio_do_atendimento;

      $momento_da_finalizacao = $chamado->get_momento_da_finalizacao();
      $objeto_date_time = new DateTime($momento_da_finalizacao, $sem_fuso_horario);
      $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);
      $momento_da_finalizacao = $objeto_date_time->format('Y-m-d H:i:s');
      $momento_da_finalizacao = $this->converter_para_horario_data_do_html($momento_da_finalizacao);
      $array_chamado['momento_da_finalizacao'] = $momento_da_finalizacao;

      /* Resumindo a descrição do chamado */
      $descricao_resumida = $chamado->get_descricao();
      if(mb_strlen($descricao_resumida) > 200){
        $descricao_resumida = substr($descricao_resumida, 0, 200);
        $ultimo_caractere = substr($descricao_resumida, -1);
        if($ultimo_caractere === ' ' or $ultimo_caractere === '.'){
          $descricao_resumida = substr($descricao_resumida, 0, 199);
        }
        $descricao_resumida .= '...';
      }
      $descricao_resumida = htmlspecialchars($descricao_resumida);
      $descricao_resumida = $this->acrescentar_quebras_de_linha_xhtml($descricao_resumida);
      $array_chamado['descricao_resumida'] = $descricao_resumida;

      $array_chamados[] = $array_chamado;
    }

    $valores['chamados'] = $array_chamados;

    return $valores;
  }

  private function calcular_quantidade_de_paginas_da_tabela_de_chamados($filtros, $quantidade_por_pagina){
    $chamados_model = new ChamadosModel();

    $array_resultado = $chamados_model->contar_chamados($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

  public function abrir_novo_chamado(){
    $chamados_model = new ChamadosModel();
    $chamado = new Chamado();

    $this->conferir_se_o_usuario_esta_logado();
    $sessao = session();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $titulo_do_chamado = trim($requisicao->post('titulo_do_chamado') ?? '');
    $descricao_do_chamado = trim($requisicao->post('descricao_do_chamado') ?? '');
    $anexos_do_chamado = trim($requisicao->post('anexos_do_chamado') ?? '');

    /* Fazendo backup do formulário */
    $backup_do_formulario['titulo_do_chamado'] = $titulo_do_chamado;
    $backup_do_formulario['descricao_do_chamado'] = $descricao_do_chamado;
    $backup_do_formulario['anexos_do_chamado'] = $anexos_do_chamado;
    $sessao->put('backup_do_formulario_da_pagina_chamados', $backup_do_formulario);
    $sessao->save();

    /* Verificando se o usuário está logado */
    if($this->get_usuario_logado() === null){
      $mensagem = 'Você precisa entrar para poder abrir um chamado.';
      $sessao->put('mensagem_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    /* Verificando se o usuário logado é do tipo cliente */
    if($this->get_usuario_logado()->get_tipo() !== 'cliente'){
      $mensagem = 'Apenas clientes podem abrir um chamado.';
      $sessao->put('mensagem_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $chamado->set_fk_usuario($this->get_usuario_logado()->get_pk_usuario());

    /* Validando o título do chamado */
    if($titulo_do_chamado === ''){
      $mensagem = 'O campo título do chamado precisa ser preenchido.';
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $minimo = $chamado->quantidade_minima_de_caracteres('titulo');
    $maximo = $chamado->quantidade_maxima_de_caracteres('titulo');
    $quantidade = mb_strlen($titulo_do_chamado);
    if($quantidade < $minimo){
      $mensagem = "O campo título do chamado precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = "O campo título do chamado não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $chamado->set_titulo($titulo_do_chamado);

    /* Validando a descrição do chamado */
    if($descricao_do_chamado === ''){
      $mensagem = 'O campo descrição do chamado precisa ser preenchido.';
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $minimo = $chamado->quantidade_minima_de_caracteres('descricao');
    $maximo = $chamado->quantidade_maxima_de_caracteres('descricao');
    $quantidade = mb_strlen($descricao_do_chamado);
    if($quantidade < $minimo){
      $mensagem = "O campo descrição do chamado precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = "O campo descrição do chamado não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $chamado->set_descricao($descricao_do_chamado);

    /* Validando os anexos do chamado */
    $maximo = $chamado->quantidade_maxima_de_caracteres('anexos');
    $quantidade = mb_strlen($anexos_do_chamado);
    if($quantidade > $maximo){
      $mensagem = "O campo anexos não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $chamado->set_anexos($anexos_do_chamado);

    /* Momento atual sem fuso horário, pois no banco de dados armazeno sem fuso horário (timezone) */
    $sem_fuso_horario = new DateTimeZone('GMT');
    $objeto_date_time = new DateTime('now', $sem_fuso_horario);
    $momento_atual = $objeto_date_time->format('Y-m-d H:i:s');
    $chamado->set_momento_da_abertura($momento_atual);

    /* Criando um novo chamado no banco de dados */
    $array_resultado = $chamados_model->inserir_chamado($chamado);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }else{
      $mensagem = 'Seu chamado foi criado com sucesso.';
      $sessao->put('mensagem_do_formulario_novo_chamado_da_pagina_chamados', $mensagem);
      $sessao->forget('backup_do_formulario_da_pagina_chamados');
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
  }

}
