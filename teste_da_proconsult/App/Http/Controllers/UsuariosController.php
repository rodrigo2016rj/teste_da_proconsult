<?php

namespace App\Http\Controllers;

use App\Models\UsuariosModel;
use App\Models\Entidades\Usuario;
use Inertia\Inertia;
use DateTime;
use DateTimeZone;

final class UsuariosController extends TemplateLayoutController{
  private const QUANTIDADE_PADRAO_POR_PAGINA = 10;

  public function carregar_pagina($redirecionar = false){
    if($redirecionar){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header('Location: /usuarios');
      die;
    }

    $this->conferir_se_o_usuario_esta_logado();
    $valores = $this->get_valores();
    $sessao = session();

    /* Especificando a página do sistema para os links e outras tags */
    $valores['template_layout']['pagina_do_sistema'] = 'usuarios';

    /* Mostrando mensagem caso exista alguma */
    if($sessao->has('mensagem_do_sistema')){
      $valores['template_layout']['mensagem'] = $sessao->get('mensagem_do_sistema');
      $sessao->forget('mensagem_do_sistema');
      $sessao->save();
    }

    /* Variável que guarda a mensagem da página começa inicialmente vazia */
    $mensagem = '';

    /* $mostrar_usuarios a princípio é true */
    $mostrar_usuarios = true;

    /* Carregando lista de tipos de usuários */
    $usuario = new Usuario();
    $valores['usuarios']['tipos_de_usuario'] = $usuario->enum_tipo();

    /* Carregando lista de quantidades por página */
    $quantidades_por_pagina = $this->criar_array_quantidades_por_pagina();
    $valores['usuarios']['quantidades_por_pagina'] = $quantidades_por_pagina;

    /* Carregando tabela de usuários */
    $valores['usuarios']['tabela_de_usuarios'] = $this->mostrar_usuarios();

    /* Verificando se o usuário está logado */
    $usuario_logado = $this->get_usuario_logado();
    if($usuario_logado === null){
      $mensagem = 'Você precisa entrar para poder acessar a página de usuários.';
      $mostrar_usuarios = false;
      unset($valores['usuarios']['tipos_de_usuario']);
      unset($valores['usuarios']['quantidades_por_pagina']);
      unset($valores['usuarios']['tabela_de_usuarios']);
    }elseif($usuario_logado->get_tipo() !== 'colaborador'){
      $mensagem = 'Você não tem permissão para acessar a página de usuários.';
      $mostrar_usuarios = false;
      unset($valores['usuarios']['tipos_de_usuario']);
      unset($valores['usuarios']['quantidades_por_pagina']);
      unset($valores['usuarios']['tabela_de_usuarios']);
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_usuarios')){
      $mensagem = $sessao->get('mensagem_da_pagina_usuarios');
      $sessao->forget('mensagem_da_pagina_usuarios');
      $sessao->save();
    }

    $valores['usuarios']['mostrar_usuarios'] = $mostrar_usuarios;
    $valores['usuarios']['mensagem_da_pagina'] = $mensagem;
    return Inertia::render('usuarios/usuarios', $valores);
  }

  public function entrar(){
    $this->entrar_padronizado();
    $this->carregar_pagina(true);
    die;
  }

  public function sair(){
    $this->sair_padronizado();
    $this->carregar_pagina(true);
    die;
  }

  private function criar_array_quantidades_por_pagina(){
    $quantidades_por_pagina['5'] = 5;
    $quantidades_por_pagina['10'] = 10;
    $quantidades_por_pagina['15'] = 15;
    $quantidades_por_pagina['20'] = 20;
    $quantidades_por_pagina['25'] = 25;
    $quantidades_por_pagina['30'] = 30;
    $quantidades_por_pagina['40'] = 40;
    $quantidades_por_pagina['50'] = 50;
    $quantidades_por_pagina['60'] = 60;
    $quantidades_por_pagina['100'] = 100;
    $quantidades_por_pagina['120'] = 120;

    return $quantidades_por_pagina;
  }

  private function mostrar_usuarios(){
    $usuarios_model = new UsuariosModel();
    $usuario = new Usuario();

    $requisicao = $this->get_requisicao();
    $valores = array();

    /* Preparando os filtros */
    $filtros = array();
    $filtro_nome = trim($requisicao->get('filtro_nome') ?? '');
    if($filtro_nome !== ''){
      $filtros['nome'] = $filtro_nome;
    }
    $valores['filtro_nome'] = $filtro_nome;

    $filtro_cpf = trim($requisicao->get('filtro_cpf') ?? '');
    if($filtro_cpf !== ''){
      $filtros['cpf'] = $filtro_cpf;
    }
    $valores['filtro_cpf'] = $filtro_cpf;

    $filtro_email = trim($requisicao->get('filtro_email') ?? '');
    if($filtro_email !== ''){
      $filtros['email'] = $filtro_email;
    }
    $valores['filtro_email'] = $filtro_email;

    $filtro_tipo_de_usuario = $requisicao->get('filtro_tipo_de_usuario');
    $array_tipos_de_usuario = $usuario->enum_tipo();
    if(isset($array_tipos_de_usuario[$filtro_tipo_de_usuario])){
      $filtros['tipo_de_usuario'] = $filtro_tipo_de_usuario;
    }else{
      $filtro_tipo_de_usuario = 'todos';
    }
    $valores['filtro_tipo_de_usuario'] = $filtro_tipo_de_usuario;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    $valores['ordem_do_nome_completo'] = '';
    $valores['ordem_do_cpf'] = '';
    $valores['ordem_do_email'] = '';
    $valores['ordem_do_momento_do_cadastro'] = '';
    $valores['ordem_do_tipo'] = '';
    switch($ordenacao){
      case 'padrao':
        break;
      case 'nome_completo_em_ordem_alfabetica':
        $valores['ordem_do_nome_completo'] = ' (A → Z)';
        break;
      case 'nome_completo_em_ordem_alfabetica_inversa':
        $valores['ordem_do_nome_completo'] = ' (Z → A)';
        break;
      case 'cpf_crescente':
        $valores['ordem_do_cpf'] = ' (0 → 9)';
        break;
      case 'cpf_decrescente':
        $valores['ordem_do_cpf'] = ' (9 → 0)';
        break;
      case 'email_em_ordem_alfabetica':
        $valores['ordem_do_email'] = ' (A → Z)';
        break;
      case 'email_em_ordem_alfabetica_inversa':
        $valores['ordem_do_email'] = ' (Z → A)';
        break;
      case 'momento_do_cadastro_em_ordem_cronologica':
        $valores['ordem_do_momento_do_cadastro'] = ' (1 → 31)';
        break;
      case 'momento_do_cadastro_em_ordem_cronologica_inversa':
        $valores['ordem_do_momento_do_cadastro'] = ' (31 → 1)';
        break;
      case 'tipo_em_ordem_alfabetica':
        $valores['ordem_do_tipo'] = ' (A → Z)';
        break;
      case 'tipo_em_ordem_alfabetica_inversa':
        $valores['ordem_do_tipo'] = ' (Z → A)';
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = (int) $requisicao->get('quantidade_por_pagina');
    $quantidades_por_pagina = $this->criar_array_quantidades_por_pagina();
    if(in_array($quantidade_por_pagina, $quantidades_por_pagina)){
      $valores['quantidade_por_pagina'] = $quantidade_por_pagina;
    }else{
      $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;
      $valores['quantidade_por_pagina'] = 'padrao';
    }

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_da_tabela_de_usuarios($filtros,
      $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores['pagina_atual'] = $pagina;
    $valores['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = max($descartar, 0);

    /* Preparando o resultado */
    $usuarios = $usuarios_model->selecionar_usuarios($filtros, $ordenacao, $quantidade_por_pagina,
      $descartar);
    $array_usuarios = array();
    foreach($usuarios as $usuario){
      $array_usuario = array();

      $array_usuario['id_do_usuario'] = $usuario->get_pk_usuario();
      $array_usuario['nome_completo'] = $usuario->get_nome_completo();
      $array_usuario['cpf'] = $usuario->get_cpf();
      $array_usuario['email'] = $usuario->get_email();

      $momento_do_cadastro = $usuario->get_momento_do_cadastro();

      /* Colocando o momento do cadastro no horário de Brasília */
      $sem_fuso_horario = new DateTimeZone('GMT');
      $objeto_date_time = new DateTime($momento_do_cadastro, $sem_fuso_horario);
      $fuso_horario_de_brasilia = new DateTimeZone('-0300');
      $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);
      $momento_do_cadastro = $objeto_date_time->format('Y-m-d H:i:s');

      $momento_do_cadastro = $this->converter_para_horario_data_do_html($momento_do_cadastro);
      $array_usuario['momento_do_cadastro'] = $momento_do_cadastro;

      $array_usuario['tipo'] = $usuario->get_tipo();

      $array_usuarios[] = $array_usuario;
    }

    $valores['usuarios'] = $array_usuarios;

    return $valores;
  }

  private function calcular_quantidade_de_paginas_da_tabela_de_usuarios($filtros, $quantidade_por_pagina){
    $usuarios_model = new UsuariosModel();

    $array_resultado = $usuarios_model->contar_usuarios($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

  public function mudar_tipo(){
    $sessao = session();
    $usuarios_model = new UsuariosModel();
    $usuario = new Usuario();

    $this->conferir_se_o_usuario_esta_logado();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $id_do_usuario = $requisicao->get('id');

    /* Verificando se o usuário está logado */
    $usuario_logado = $this->get_usuario_logado();
    if($usuario_logado === null){
      $mensagem = 'O tipo do usuário não foi modificado.';
      $mensagem .= ' Você precisa entrar para poder mudar o tipo de um usuário.';
      $sessao->put('mensagem_da_pagina_usuarios', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    /* Verificando se o usuário logado é um colaborador */
    if($usuario_logado->get_tipo() !== 'colaborador'){
      $mensagem = 'O tipo do usuário não foi modificado.';
      $mensagem .= ' Você não tem permissão para mudar o tipo de um usuário.';
      $sessao->put('mensagem_da_pagina_usuarios', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    /* Validando o ID de usuário informado na URL */
    $usuario = new Usuario();
    if(!is_numeric($id_do_usuario) or $id_do_usuario <= 0 or floor($id_do_usuario) != $id_do_usuario){
      $mensagem = 'O tipo do usuário não foi modificado.';
      $mensagem .= ' ID inválido, o ID do usuário precisa ser um número natural maior que zero.';
      $sessao->put('mensagem_da_pagina_usuarios', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    /* Verificando se o usuário logado não é o próprio usuário alvo */
    if($usuario_logado->get_pk_usuario() == $id_do_usuario){
      $mensagem = 'O tipo do usuário não foi modificado.';
      $mensagem .= ' Você não pode mudar o seu tipo de usuário.';
      $sessao->put('mensagem_da_pagina_usuarios', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    $usuario_alvo = null;

    /* Verificando se o id informado corresponde ao id de algum usuário no banco de dados */
    $array_resultado = $usuarios_model->selecionar_usuario_pela_pk($id_do_usuario);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O tipo do usuário não foi modificado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_usuarios', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }else{
      $usuario_alvo = $array_resultado[0];

      /* Verificando se o usuário logado é mais antigo que o usuário alvo */
      $sem_fuso_horario = new DateTimeZone('GMT');
      $momento_do_cadastro_do_usuario_alvo = $usuario_alvo->get_momento_do_cadastro();
      $momento_do_cadastro_do_usuario_alvo = new DateTime($momento_do_cadastro_do_usuario_alvo, $sem_fuso_horario);
      $momento_do_cadastro_do_usuario_logado = $usuario_logado->get_momento_do_cadastro();
      $momento_do_cadastro_do_usuario_logado = new DateTime($momento_do_cadastro_do_usuario_logado, $sem_fuso_horario);
      if($momento_do_cadastro_do_usuario_logado >= $momento_do_cadastro_do_usuario_alvo){
        $mensagem = 'O tipo do usuário não foi modificado.';
        $mensagem .= ' Você não pode mudar o tipo de um usuário mais antigo que você.';
        $sessao->put('mensagem_da_pagina_usuarios', $mensagem);
        $sessao->save();
        $this->carregar_pagina(true);
        die;
      }
    }

    /* Alterando o tipo do usuário alvo */
    switch($usuario_alvo->get_tipo()){
      case 'cliente':
        $usuario_alvo->set_tipo('colaborador');
        break;
      case 'colaborador':
        $usuario_alvo->set_tipo('cliente');
        break;
    }
    $array_resultado = $usuarios_model->editar_tipo_de_usuario($usuario_alvo);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O tipo do usuário não foi modificado.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $sessao->put('mensagem_da_pagina_usuarios', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }else{
      $mensagem = 'O tipo do usuário foi modificado com sucesso.';
      $sessao->put('mensagem_da_pagina_usuarios', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
  }

}
