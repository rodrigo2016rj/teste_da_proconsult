<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\TemplateLayoutModel;

class TemplateLayoutController extends Controller{
  /* Armazena informações do array que guarda os valores da view. */
  private $valores;

  /* Armazena informações do usuário logado. */
  private $usuario_logado;

  /* Armazena o objeto da requisição. */
  private $requisicao;

  public function __construct(Request $requisicao){
    $this->requisicao = $requisicao;
  }

  protected final function get_valores(){
    //Só use este método após chamar o conferir_se_o_usuario_esta_logado
    return $this->valores;
  }

  protected final function get_usuario_logado(){
    //Só use este método após chamar o conferir_se_o_usuario_esta_logado
    return $this->usuario_logado;
  }

  protected final function get_requisicao(){
    return $this->requisicao;
  }

  /** ---------------------------------------------------------------------------------------------
    Confere se o usuário está logado e melhora o array valores. */
  protected final function conferir_se_o_usuario_esta_logado(){
    $template_layout_model = new TemplateLayoutModel();

    $sessao = session();

    $this->valores['template_layout']['chave_anti_csrf'] = csrf_token();
    $this->valores['template_layout']['pagina_do_sistema'] = '';
    $this->valores['template_layout']['id_referencia'] = '';
    $this->valores['template_layout']['mensagem'] = '';
    $this->valores['template_layout']['usuario_esta_logado'] = false;
    $this->valores['template_layout']['id_do_usuario_logado'] = null;
    $this->valores['template_layout']['nome_do_usuario_logado'] = null;

    $this->usuario_logado = null;
    if($sessao->has('id_do_usuario_logado')){
      $id_do_usuario_logado = $sessao->get('id_do_usuario_logado');
      $array_resultado = $template_layout_model->seleciona_usuario_pela_pk($id_do_usuario_logado);

      if(isset($array_resultado['mensagem_do_model'])){
        //Aqui eu posso colocar para guardar um registro de tentativa suspeita de login por exemplo.
      }else{
        $this->usuario_logado = $array_resultado[0];
        $this->valores['template_layout']['usuario_esta_logado'] = true;
        $this->valores['template_layout']['id_do_usuario_logado'] = $this->usuario_logado->get_pk_usuario();
        $this->valores['template_layout']['nome_do_usuario_logado'] = $this->usuario_logado->get_nome_completo();
      }
    }
  }

  /** ---------------------------------------------------------------------------------------------
    Função padrão para o usuário entrar na conta (login). */
  protected final function entrar_padronizado(){
    $template_layout_model = new TemplateLayoutModel();

    $sessao = session();

    /* Obtendo valores do formulário */
    $requisicao = $this->requisicao;
    $email = trim($requisicao->post('email') ?? '');
    $senha = $requisicao->post('senha');

    if($email === ''){
      $mensagem = 'O campo e-mail precisa ser preenchido.';
      $sessao->put('mensagem_do_sistema', $mensagem);
      $sessao->save();
      return false;
    }
    if($senha === null or $senha === ''){
      $mensagem = 'O campo senha precisa ser preenchido.';
      $sessao->put('mensagem_do_sistema', $mensagem);
      $sessao->save();
      return false;
    }

    $array_resultado = $template_layout_model->seleciona_usuario_para_fazer_o_login($email);

    if(isset($array_resultado['mensagem_do_model'])){
      $sessao->put('mensagem_do_sistema', $array_resultado['mensagem_do_model']);
      $sessao->save();
      return false;
    }else{
      $usuario = $array_resultado[0];
      if(password_verify($senha, $usuario->get_senha())){
        $sessao->put('id_do_usuario_logado', $usuario->get_pk_usuario());
        $sessao->save();
      }else{
        $mensagem = 'A senha digitada não está correta.';
        $sessao->put('mensagem_do_sistema', $mensagem);
        $sessao->save();
        return false;
      }
    }

    return true;
  }

  /** ---------------------------------------------------------------------------------------------
    Função padrão para o usuário sair da conta (logout). */
  protected final function sair_padronizado(){
    $sessao = session();
    $sessao->forget('id_do_usuario_logado');
    $sessao->save();
  }

  /** ---------------------------------------------------------------------------------------------
    Criptografa a senha do usuário. */
  protected final function criptografar_senha_do_usuario($senha){
    $senha_criptografada = password_hash($senha, PASSWORD_DEFAULT);

    return $senha_criptografada;
  }

  /** ---------------------------------------------------------------------------------------------
    Acrescenta quebras de linha no padrão XHTML. */
  protected function acrescentar_quebras_de_linha_xhtml($texto){
    //Armazena em array todos os padrões de quebra de linha de sistemas operacionais diferentes
    $tipos_de_quebras_de_sistemas_operacionais = array("\r\n", "\r", "\n");
    //Substitui quebras de linha presentes na string por: termina parágrafo </p> começa parágrafo <p>
    $texto_modificado = str_replace($tipos_de_quebras_de_sistemas_operacionais, '</p><p>', $texto);
    //Substitui parágrafo vazio por: quebra de linha <br/>
    $texto_resultante = str_replace('<p></p>', '<br/>', $texto_modificado);
    //Retorna o texto resultante dentro da tag <p></p>
    return "<p>$texto_resultante</p>";
  }

  /** ---------------------------------------------------------------------------------------------
    Converte yyyy-MM-dd para: dd/MM/yyyy */
  protected function converter_para_data_do_html($data){
    if(!preg_match('/^\d{4}-\d{2}-\d{2}$/', $data)){
      //Caso não venha no formato certo, retorna a string sem conversão.
      return $data;
    }
    $ano = substr($data, 0, 4);
    $mes = substr($data, 5, 2);
    $dia = substr($data, 8, 2);
    return "$dia/$mes/$ano";
  }

  /** ---------------------------------------------------------------------------------------------
    Converte yyyy-MM-dd xx:yy:zz para: dd/MM/yyyy às xxhyy */
  protected function converter_para_horario_data_do_html($string){
    if(!preg_match('/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/', $string)){
      //Caso não venha no formato certo, retorna a string sem conversão.
      return $string;
    }
    $ano = substr($string, 0, 4);
    $mes = substr($string, 5, 2);
    $dia = substr($string, 8, 2);
    $horas = substr($string, 11, 2);
    $minutos = substr($string, 14, 2);
    return "$dia/$mes/$ano às ".$horas.'h'.$minutos;
  }

}
