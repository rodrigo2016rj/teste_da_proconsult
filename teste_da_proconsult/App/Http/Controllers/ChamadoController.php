<?php

namespace App\Http\Controllers;

use App\Models\ChamadoModel;
use App\Models\Entidades\Chamado;
use App\Models\Entidades\Resposta;
use Inertia\Inertia;
use DateTime;
use DateTimeZone;

final class ChamadoController extends TemplateLayoutController{

  public function carregar_pagina($redirecionar_com_id = false){
    if($redirecionar_com_id === 'pagina_inicial'){
      //Redireciona para a página inicial caso seja necessário.
      header('Location: /pagina_inicial');
      die;
    }elseif($redirecionar_com_id !== false){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header("Location: /chamado?id=$redirecionar_com_id");
      die;
    }

    $this->conferir_se_o_usuario_esta_logado();
    $valores = $this->get_valores();
    $sessao = session();

    /* Especificando a página do sistema para os links e outras tags */
    $valores['template_layout']['pagina_do_sistema'] = 'chamado';

    /* Mostrando mensagem caso exista alguma */
    if($sessao->has('mensagem_do_sistema')){
      $valores['template_layout']['mensagem'] = $sessao->get('mensagem_do_sistema');
      $sessao->forget('mensagem_do_sistema');
      $sessao->save();
    }

    /* Variável que guarda a mensagem da página começa inicialmente vazia */
    $mensagem = '';

    /* $mostrar_chamado a princípio é true */
    $mostrar_chamado = true;

    /* $mostrar_formulario_de_resposta a princípio é true */
    $mostrar_formulario_de_resposta = true;

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['chamado']['id_do_chamado'] = '';
    $valores['chamado']['titulo'] = '';
    $valores['chamado']['status'] = '';
    $valores['chamado']['nome_do_cliente'] = '';
    $valores['chamado']['momento_da_abertura'] = '';
    $valores['chamado']['descricao'] = '';
    $valores['chamado']['anexos'] = '';

    $chamado_model = new ChamadoModel();

    /* Validando o ID de chamado informado na URL */
    $requisicao = $this->get_requisicao();
    $pk_chamado = $requisicao->get('id');
    $id_do_cliente = null;
    $chamado = new Chamado();
    if(!is_numeric($pk_chamado) or $pk_chamado <= 0 or floor($pk_chamado) != $pk_chamado){
      $mensagem = 'ID inválido, o ID do chamado precisa ser um número natural maior que zero.';
      $mostrar_chamado = false;
    }else{
      $valores['template_layout']['id_referencia'] = $pk_chamado;

      /* Consultando e mostrando informações do chamado */
      $array_resultado = $chamado_model->selecionar_chamado($pk_chamado);
      if(isset($array_resultado['mensagem_do_model'])){
        $mensagem = $array_resultado['mensagem_do_model'];
        $mostrar_chamado = false;
      }else{
        $chamado = $array_resultado[0];

        $valores['chamado']['id_do_chamado'] = $chamado->get_pk_chamado();
        $valores['chamado']['titulo'] = $chamado->get_titulo();
        $valores['chamado']['status'] = $chamado->enum_status()[$chamado->get_status()];

        /* Informações do cliente que abriu o chamado */
        $id_do_cliente = $chamado->get_usuario()->get_pk_usuario();
        $valores['chamado']['nome_do_cliente'] = $chamado->get_usuario()->get_nome_completo();

        /* Verificando se o chamado já foi finalizado */
        if($chamado->get_status() === 'finalizado'){
          $mostrar_formulario_de_resposta = false;
        }

        /* Colocando o momento no horário de Brasília */
        $sem_fuso_horario = new DateTimeZone('GMT');
        $fuso_horario_de_brasilia = new DateTimeZone('-0300');
        $momento_da_abertura = $chamado->get_momento_da_abertura();
        $objeto_date_time = new DateTime($momento_da_abertura, $sem_fuso_horario);
        $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);
        $momento_da_abertura = $objeto_date_time->format('Y-m-d H:i:s');
        $momento_da_abertura = $this->converter_para_horario_data_do_html($momento_da_abertura);
        $valores['chamado']['momento_da_abertura'] = $momento_da_abertura;

        /* Descrição do chamado com quebras de linha */
        $descricao = $chamado->get_descricao();
        $descricao = htmlspecialchars($descricao);
        $descricao = $this->acrescentar_quebras_de_linha_xhtml($descricao);
        $valores['chamado']['descricao'] = $descricao;

        /* Endereço dos anexos */
        $valores['chamado']['anexos'] = array();
        $anexos = $chamado->get_anexos();
        if($anexos){
          $anexos = htmlspecialchars($anexos);
          $anexos = str_replace("\r\n", "\n", $anexos);
          $anexos = str_replace("\r", "\n", $anexos);
          $array_anexos = explode("\n", $anexos);
          for($i = 0; $i < count($array_anexos); $i++){
            if(filter_var($array_anexos[$i], FILTER_VALIDATE_URL)){
              $valores['chamado']['anexos'][] = $array_anexos[$i];
            }
          }
        }
      }
    }

    /* Carregando lista de respostas */
    $valores['chamado']['lista_de_respostas'] = $this->mostrar_respostas($pk_chamado);

    /* Verificando se o usuário está logado */
    $usuario_logado = $this->get_usuario_logado();
    if($usuario_logado === null){
      $mensagem = 'Você precisa entrar para poder acessar a página do chamado.';
      unset($valores['chamado']['id_do_chamado']);
      unset($valores['chamado']['titulo']);
      unset($valores['chamado']['status']);
      unset($valores['chamado']['nome_do_cliente']);
      unset($valores['chamado']['momento_da_abertura']);
      unset($valores['chamado']['descricao']);
      unset($valores['chamado']['anexos']);
      unset($valores['chamado']['lista_de_respostas']);
      $mostrar_chamado = false;
    }elseif($usuario_logado->get_tipo() !== 'colaborador'
      and $usuario_logado->get_pk_usuario() !== $id_do_cliente){
      $mensagem = 'Você precisa ser um colaborador ou o cliente que abriu o chamado para poder';
      $mensagem .= ' acessar a página do chamado.';
      unset($valores['chamado']['id_do_chamado']);
      unset($valores['chamado']['titulo']);
      unset($valores['chamado']['status']);
      unset($valores['chamado']['nome_do_cliente']);
      unset($valores['chamado']['momento_da_abertura']);
      unset($valores['chamado']['descricao']);
      unset($valores['chamado']['anexos']);
      unset($valores['chamado']['lista_de_respostas']);
      $mostrar_chamado = false;
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_chamado')){
      $mensagem = $sessao->get('mensagem_da_pagina_chamado');
      $sessao->forget('mensagem_da_pagina_chamado');
      $sessao->save();
    }

    $valores['chamado']['mostrar_chamado'] = $mostrar_chamado;
    $valores['chamado']['mostrar_formulario_de_resposta'] = $mostrar_formulario_de_resposta;
    $valores['chamado']['mensagem_da_pagina'] = $mensagem;
    return Inertia::render('chamado/chamado', $valores);
  }

  public function entrar(){
    $this->entrar_padronizado();
    $requisicao = $this->get_requisicao();
    $pk_chamado = $requisicao->get('id');
    if(is_numeric($pk_chamado) and $pk_chamado > 0 and floor($pk_chamado) == $pk_chamado){
      $this->carregar_pagina($pk_chamado);
    }else{
      $this->carregar_pagina('pagina_inicial');
    }
    die;
  }

  public function sair(){
    $this->sair_padronizado();
    $requisicao = $this->get_requisicao();
    $pk_chamado = $requisicao->get('id');
    if(is_numeric($pk_chamado) and $pk_chamado > 0 and floor($pk_chamado) == $pk_chamado){
      $this->carregar_pagina($pk_chamado);
    }else{
      $this->carregar_pagina('pagina_inicial');
    }
    die;
  }

  private function mostrar_respostas($pk_chamado){
    $chamado_model = new ChamadoModel();

    $valores = array();

    /* Preparando o resultado */
    $respostas = $chamado_model->selecionar_respostas($pk_chamado);
    $array_respostas = array();
    foreach($respostas as $resposta){
      $array_resposta = array();

      /* Informações do autor da resposta */
      $array_resposta['autor'] = $resposta->get_usuario()->get_nome_completo();

      /* Colocando o momento no horário de Brasília */
      $sem_fuso_horario = new DateTimeZone('GMT');
      $fuso_horario_de_brasilia = new DateTimeZone('-0300');
      $momento_do_envio = $resposta->get_momento_do_envio();
      $objeto_date_time = new DateTime($momento_do_envio, $sem_fuso_horario);
      $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);
      $momento_do_envio = $objeto_date_time->format('Y-m-d H:i:s');
      $momento_do_envio = $this->converter_para_horario_data_do_html($momento_do_envio);
      $array_resposta['momento_do_envio'] = $momento_do_envio;

      /* Mensagem da resposta com quebras de linha */
      $mensagem = $resposta->get_mensagem();
      $mensagem = htmlspecialchars($mensagem);
      $mensagem = $this->acrescentar_quebras_de_linha_xhtml($mensagem);
      $array_resposta['mensagem'] = $mensagem;

      /* Endereço dos anexos */
      $array_resposta['anexos'] = array();
      $anexos = $resposta->get_anexos();
      if($anexos){
        $anexos = htmlspecialchars($anexos);
        $anexos = str_replace("\r\n", "\n", $anexos);
        $anexos = str_replace("\r", "\n", $anexos);
        $array_anexos = explode("\n", $anexos);
        for($i = 0; $i < count($array_anexos); $i++){
          if(filter_var($array_anexos[$i], FILTER_VALIDATE_URL)){
            $array_resposta['anexos'][] = $array_anexos[$i];
          }
        }
      }

      $array_respostas[] = $array_resposta;
    }

    $valores['respostas'] = $array_respostas;

    return $valores;
  }

  public function responder_via_ajax(){
    $chamado_model = new ChamadoModel();
    $resposta = new Resposta();

    $this->conferir_se_o_usuario_esta_logado();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $pk_chamado = $requisicao->post('id_do_chamado');
    $mensagem_da_resposta = trim($requisicao->post('mensagem_da_resposta') ?? '');
    $anexos_da_resposta = trim($requisicao->post('anexos_da_resposta') ?? '');

    /* Verificando se o usuário está logado */
    if($this->get_usuario_logado() === null){
      $mensagem = 'Você precisa entrar para poder responder um chamado.';
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }

    /* Validando id do chamado (pk_chamado) */
    if(!is_numeric($pk_chamado) or $pk_chamado <= 0 or floor($pk_chamado) != $pk_chamado){
      $mensagem = 'O ID do chamado precisa ser um número natural maior que zero.';
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }

    $array_resultado = $chamado_model->selecionar_chamado($pk_chamado);
    $id_do_cliente = null;
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }else{
      $chamado = $array_resultado[0];

      if($chamado->get_status() === 'finalizado'){
        $mensagem = 'Não é possível responder a este chamado, pois este chamado já foi finalizado.';
        $retorno['mensagem_de_falha'] = $mensagem;
        echo json_encode($retorno);
        die;
      }

      $id_do_cliente = $chamado->get_fk_usuario();
    }
    $resposta->set_fk_chamado($pk_chamado);

    /* Verificando se o usuário logado é do tipo colaborador ou se é o cliente que abriu o chamado */
    if($this->get_usuario_logado()->get_tipo() !== 'colaborador'
      and $this->get_usuario_logado()->get_pk_usuario() !== $id_do_cliente){
      $mensagem = 'Você precisa ser um colaborador ou o cliente que abriu o chamado para poder';
      $mensagem .= ' responder um chamado.';
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }
    $resposta->set_fk_usuario($this->get_usuario_logado()->get_pk_usuario());

    /* Validando a mensagem da resposta */
    if($mensagem_da_resposta === ''){
      $mensagem = 'O campo mensagem da resposta precisa ser preenchido.';
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }
    $minimo = $resposta->quantidade_minima_de_caracteres('mensagem');
    $maximo = $resposta->quantidade_maxima_de_caracteres('mensagem');
    $quantidade = mb_strlen($mensagem_da_resposta);
    if($quantidade < $minimo){
      $mensagem = "O campo mensagem da resposta precisa ter no mínimo $minimo caracteres.";
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = "O campo mensagem da resposta não pode ultrapassar $maximo caracteres.";
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }
    $resposta->set_mensagem($mensagem_da_resposta);

    /* Validando os anexos da resposta */
    $maximo = $resposta->quantidade_maxima_de_caracteres('anexos');
    $quantidade = mb_strlen($anexos_da_resposta);
    if($quantidade > $maximo){
      $mensagem = "O campo anexos não pode ultrapassar $maximo caracteres.";
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }
    $resposta->set_anexos($anexos_da_resposta);

    /* Momento atual sem fuso horário, pois no banco de dados armazeno sem fuso horário (timezone) */
    $sem_fuso_horario = new DateTimeZone('GMT');
    $objeto_date_time = new DateTime('now', $sem_fuso_horario);
    $momento_atual = $objeto_date_time->format('Y-m-d H:i:s');
    $resposta->set_momento_do_envio($momento_atual);

    /* Registrando a resposta no banco de dados */
    $array_resultado = $chamado_model->registrar_resposta($resposta);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }else{
      $retorno['lista_de_respostas'] = $this->mostrar_respostas($pk_chamado);

      /* Se o chamado estiver com o status aberto, o status irá mudar para em atendimento */
      if($chamado->get_status() === 'aberto'){
        $chamado_model->atualizar_o_status_do_chamado_para_em_atendimento($pk_chamado, $momento_atual);
      }

      $mensagem = 'Sua resposta foi enviada com sucesso.';
      $retorno['mensagem_de_sucesso'] = $mensagem;
      echo json_encode($retorno);
      die;
    }
  }

  public function finalizar_chamado_via_ajax(){
    $chamado_model = new ChamadoModel();
    $resposta = new Resposta();

    $this->conferir_se_o_usuario_esta_logado();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $pk_chamado = $requisicao->post('id_do_chamado');

    /* Verificando se o usuário está logado */
    if($this->get_usuario_logado() === null){
      $mensagem = 'Você precisa entrar para poder finalizar um chamado.';
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }

    /* Validando id do chamado (pk_chamado) */
    if(!is_numeric($pk_chamado) or $pk_chamado <= 0 or floor($pk_chamado) != $pk_chamado){
      $mensagem = 'O ID do chamado precisa ser um número natural maior que zero.';
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }

    $array_resultado = $chamado_model->selecionar_chamado($pk_chamado);
    $id_do_cliente = null;
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }else{
      $chamado = $array_resultado[0];

      if($chamado->get_status() === 'finalizado'){
        $mensagem = 'Não é possível finalizar este chamado, pois este chamado já foi finalizado.';
        $retorno['mensagem_de_falha'] = $mensagem;
        echo json_encode($retorno);
        die;
      }

      $id_do_cliente = $chamado->get_fk_usuario();
    }
    $resposta->set_fk_chamado($pk_chamado);

    /* Verificando se o usuário logado é do tipo colaborador ou se é o cliente que abriu o chamado */
    if($this->get_usuario_logado()->get_tipo() !== 'colaborador'
      and $this->get_usuario_logado()->get_pk_usuario() !== $id_do_cliente){
      $mensagem = 'Você precisa ser um colaborador ou o cliente que abriu o chamado para poder';
      $mensagem .= ' finalizar um chamado.';
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }
    $resposta->set_fk_usuario($this->get_usuario_logado()->get_pk_usuario());

    $mensagem_da_resposta = 'Chamado finalizado.';
    $resposta->set_mensagem($mensagem_da_resposta);

    /* Momento atual sem fuso horário, pois no banco de dados armazeno sem fuso horário (timezone) */
    $sem_fuso_horario = new DateTimeZone('GMT');
    $objeto_date_time = new DateTime('now', $sem_fuso_horario);
    $momento_atual = $objeto_date_time->format('Y-m-d H:i:s');
    $resposta->set_momento_do_envio($momento_atual);

    /* Finalizando o chamado */
    $array_resultado = $chamado_model->atualizar_o_status_do_chamado_para_finalizado($pk_chamado, $momento_atual);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $retorno['mensagem_de_falha'] = $mensagem;
      echo json_encode($retorno);
      die;
    }else{
      $mensagem = 'O chamado foi finalizado com sucesso.';
      $retorno['mensagem_de_sucesso'] = $mensagem;

      /* Registrando a resposta automática no banco de dados */
      $chamado_model->registrar_resposta($resposta);
      $retorno['lista_de_respostas'] = $this->mostrar_respostas($pk_chamado);

      echo json_encode($retorno);
      die;
    }
  }

}
