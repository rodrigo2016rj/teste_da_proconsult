<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use PDOException;

final class CadastreSeModel{

  public function verificar_disponibilidade_de_nome_completo($nome_completo){
    $query = DB::table('usuario');
    $query = $query->addSelect('pk_usuario');
    $query = $query->where('nome_completo', '=', $nome_completo);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) > 0){
      $mensagem_do_model = 'O nome informado já havia sido cadastrado. Por favor utilize outro';
      $mensagem_do_model .= ' nome para poder se cadastrar ou verifique se você já não está';
      $mensagem_do_model .= ' cadastrado.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function verificar_disponibilidade_de_cpf($cpf){
    $query = DB::table('usuario');
    $query = $query->addSelect('pk_usuario');
    $query = $query->where('cpf', '=', $cpf);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) > 0){
      $mensagem_do_model = 'O CPF informado já havia sido cadastrado. Por favor verifique se você';
      $mensagem_do_model .= ' digitou seu CPF corretamente ou se você já não está cadastrado.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function verificar_disponibilidade_de_email($email){
    $query = DB::table('usuario');
    $query = $query->addSelect('pk_usuario');
    $query = $query->where('email', '=', $email);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) > 0){
      $mensagem_do_model = 'O e-mail informado já havia sido cadastrado. Por favor verifique se você';
      $mensagem_do_model .= ' digitou seu e-mail corretamente ou se você já não está cadastrado.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function cadastrar_usuario($usuario){
    $insert['nome_completo'] = $usuario->get_nome_completo();
    $insert['cpf'] = $usuario->get_cpf();
    $insert['email'] = $usuario->get_email();
    $insert['senha'] = $usuario->get_senha();
    $insert['momento_do_cadastro'] = $usuario->get_momento_do_cadastro();

    $array_resultado = array();

    try{
      DB::table('usuario')->insert($insert);
      $array_resultado['id_do_usuario'] = DB::getPdo()->lastInsertId();
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        case 1062:
          $mensagem = 'Já existe um usuário cadastrado com uma ou mais destas informações.';
          $array_resultado['mensagem_do_model'] = $mensagem;
          break;
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
