<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Chamado;
use App\Models\Entidades\Usuario;
use PDOException;

final class ChamadosModel{

  public function selecionar_chamados($filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('chamado');
    $query = $query->addSelect('pk_chamado');
    $query = $query->addSelect('chamado.fk_usuario');
    $query = $query->addSelect('chamado.titulo');
    $query = $query->addSelect('chamado.descricao');
    $query = $query->addSelect('chamado.anexos');
    $query = $query->addSelect('chamado.status');
    $query = $query->addSelect('chamado.momento_da_abertura');
    $query = $query->addSelect('chamado.momento_do_inicio_do_atendimento');
    $query = $query->addSelect('chamado.momento_da_finalizacao');
    $query = $query->addSelect('pk_usuario');
    $query = $query->addSelect('usuario.nome_completo AS nome_completo_do_cliente');
    $query = $query->addSelect('usuario.email AS email_do_cliente');

    $query = $query->join('usuario', 'fk_usuario', '=', 'pk_usuario'); //INNER JOIN

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_usuario':
          $query = $query->where('usuario.nome_completo', 'LIKE', "%$valor%");
          break;
        case 'titulo_do_chamado':
          $query = $query->where('chamado.titulo', 'LIKE', "%$valor%");
          break;
        case 'status_do_chamado':
          $query = $query->where('chamado.status', '=', $valor);
          break;
        case 'cpf_do_usuario':
          $query = $query->where('usuario.cpf', '=', $valor);
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('pk_chamado', 'DESC');
        break;
      case 'momento_da_abertura_em_ordem_cronologica':
        $query = $query->orderBy('chamado.momento_da_abertura', 'ASC');
        $query = $query->orderBy('pk_chamado', 'DESC');
        break;
      case 'momento_da_abertura_em_ordem_cronologica_inversa':
        $query = $query->orderBy('chamado.momento_da_abertura', 'DESC');
        $query = $query->orderBy('pk_chamado', 'DESC');
        break;
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $valores = (array) $objeto_generico;

      $chamado = new Chamado();
      $chamado->set_pk_chamado($valores['pk_chamado']);
      $chamado->set_fk_usuario($valores['fk_usuario']);
      $chamado->set_titulo($valores['titulo']);
      $chamado->set_descricao($valores['descricao']);
      $chamado->set_anexos($valores['anexos']);
      $chamado->set_status($valores['status']);
      $chamado->set_momento_da_abertura($valores['momento_da_abertura']);
      $chamado->set_momento_do_inicio_do_atendimento($valores['momento_do_inicio_do_atendimento']);
      $chamado->set_momento_da_finalizacao($valores['momento_da_finalizacao']);

      $usuario = new Usuario();
      $usuario->set_pk_usuario($valores['pk_usuario']);
      $usuario->set_nome_completo($valores['nome_completo_do_cliente']);
      $usuario->set_email($valores['email_do_cliente']);

      $chamado->set_usuario($usuario);

      $array_melhorado[] = $chamado;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_chamados($filtros){
    $query = DB::table('chamado');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));

    $query = $query->join('usuario', 'fk_usuario', '=', 'pk_usuario'); //INNER JOIN

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_usuario':
          $query = $query->where('usuario.nome_completo', 'LIKE', "%$valor%");
          break;
        case 'titulo_do_chamado':
          $query = $query->where('chamado.titulo', 'LIKE', "%$valor%");
          break;
        case 'status_do_chamado':
          $query = $query->where('chamado.status', '=', $valor);
          break;
        case 'cpf_do_usuario':
          $query = $query->where('usuario.cpf', '=', $valor);
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function inserir_chamado($chamado){
    $insert['fk_usuario'] = $chamado->get_fk_usuario();
    $insert['titulo'] = $chamado->get_titulo();
    $insert['descricao'] = $chamado->get_descricao();
    $insert['anexos'] = $chamado->get_anexos();
    $insert['momento_da_abertura'] = $chamado->get_momento_da_abertura();

    $array_resultado = array();

    try{
      DB::table('chamado')->insert($insert);
      $array_resultado['id_do_chamado'] = DB::getPdo()->lastInsertId();
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
