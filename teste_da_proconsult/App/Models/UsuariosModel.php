<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Usuario;
use PDOException;

final class UsuariosModel{

  public function selecionar_usuarios($filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('usuario');
    $query = $query->addSelect('pk_usuario');
    $query = $query->addSelect('nome_completo');
    $query = $query->addSelect('cpf');
    $query = $query->addSelect('email');
    $query = $query->addSelect('momento_do_cadastro');
    $query = $query->addSelect('tipo');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome':
          $query = $query->where('nome_completo', 'LIKE', "%$valor%");
          break;
        case 'cpf':
          $query = $query->where('cpf', '=', $valor);
          break;
        case 'email':
          $query = $query->where('email', 'LIKE', "%$valor%");
          break;
        case 'tipo_de_usuario':
          $query = $query->where('tipo', '=', $valor);
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('pk_usuario', 'DESC');
        break;
      case 'nome_completo_em_ordem_alfabetica':
        $query = $query->orderBy('nome_completo', 'ASC');
        break;
      case 'nome_completo_em_ordem_alfabetica_inversa':
        $query = $query->orderBy('nome_completo', 'DESC');
        break;
      case 'cpf_crescente':
        $query = $query->orderBy('cpf', 'ASC');
        break;
      case 'cpf_decrescente':
        $query = $query->orderBy('cpf', 'DESC');
        break;
      case 'email_em_ordem_alfabetica':
        $query = $query->orderBy('email', 'ASC');
        break;
      case 'email_em_ordem_alfabetica_inversa':
        $query = $query->orderBy('email', 'DESC');
        break;
      case 'momento_do_cadastro_em_ordem_cronologica':
        $query = $query->orderBy('momento_do_cadastro', 'ASC');
        break;
      case 'momento_do_cadastro_em_ordem_cronologica_inversa':
        $query = $query->orderBy('momento_do_cadastro', 'DESC');
        break;
      case 'tipo_em_ordem_alfabetica':
        $query = $query->orderByRaw('CAST(tipo AS CHAR) ASC');
        $query = $query->orderBy('pk_usuario', 'DESC');
        break;
      case 'tipo_em_ordem_alfabetica_inversa':
        $query = $query->orderByRaw('CAST(tipo AS CHAR) DESC');
        $query = $query->orderBy('pk_usuario', 'DESC');
        break;
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $valores = (array) $objeto_generico;
      $usuario = new Usuario($valores);
      $array_melhorado[] = $usuario;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_usuarios($filtros){
    $query = DB::table('usuario');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome':
          $query = $query->where('nome_completo', 'LIKE', "%$valor%");
          break;
        case 'cpf':
          $query = $query->where('cpf', '=', $valor);
          break;
        case 'email':
          $query = $query->where('email', 'LIKE', "%$valor%");
          break;
        case 'tipo_de_usuario':
          $query = $query->where('tipo', '=', $valor);
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function selecionar_usuario_pela_pk($pk_usuario){
    $query = DB::table('usuario');
    $query = $query->addSelect('pk_usuario');
    $query = $query->addSelect('nome_completo');
    $query = $query->addSelect('cpf');
    $query = $query->addSelect('email');
    $query = $query->addSelect('momento_do_cadastro');
    $query = $query->addSelect('tipo');

    $query = $query->where('pk_usuario', '=', $pk_usuario);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = "Nenhum usuário com ID $pk_usuario foi encontrado no banco de dados";
      $mensagem_do_model .= ' do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $valores = (array) $array_resultado[0];
      $usuario = new Usuario($valores);
      $array_melhorado[] = $usuario;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function editar_tipo_de_usuario($usuario){
    $update['tipo'] = $usuario->get_tipo();

    $array_resultado = array();

    try{
      DB::table('usuario')->where('pk_usuario', '=', $usuario->get_pk_usuario())->update($update);
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
