<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Usuario;

final class TemplateLayoutModel{

  public function seleciona_usuario_pela_pk($pk_usuario){
    $query = DB::table('usuario');
    $query = $query->addSelect('pk_usuario');
    $query = $query->addSelect('nome_completo');
    $query = $query->addSelect('cpf');
    $query = $query->addSelect('email');
    $query = $query->addSelect('senha');
    $query = $query->addSelect('momento_do_cadastro');
    $query = $query->addSelect('tipo');
    $query = $query->where('pk_usuario', '=', $pk_usuario);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) == 0){
      $mensagem_do_model = 'Este usuário não foi encontrado no banco de dados deste sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $valores = (array) $array_resultado[0];
      $usuario = new Usuario($valores);
      $array_melhorado[] = $usuario;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function seleciona_usuario_para_fazer_o_login($email){
    $query = DB::table('usuario');
    $query = $query->addSelect('pk_usuario');
    $query = $query->addSelect('senha');
    $query = $query->where('email', '=', $email);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) == 0){
      $mensagem_do_model = 'Não existe nenhum usuário cadastrado com o e-mail informado.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $valores = (array) $array_resultado[0];
      $usuario = new Usuario($valores);
      $array_melhorado[] = $usuario;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

}
