<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Chamado;
use App\Models\Entidades\Usuario;
use App\Models\Entidades\Resposta;
use PDOException;

final class ChamadoModel{

  public function selecionar_chamado($pk_chamado){
    $query = DB::table('chamado');
    $query = $query->addSelect('pk_chamado');
    $query = $query->addSelect('chamado.fk_usuario');
    $query = $query->addSelect('chamado.titulo');
    $query = $query->addSelect('chamado.descricao');
    $query = $query->addSelect('chamado.anexos');
    $query = $query->addSelect('chamado.status');
    $query = $query->addSelect('chamado.momento_da_abertura');
    $query = $query->addSelect('chamado.momento_do_inicio_do_atendimento');
    $query = $query->addSelect('chamado.momento_da_finalizacao');
    $query = $query->addSelect('pk_usuario');
    $query = $query->addSelect('usuario.nome_completo AS nome_completo_do_cliente');
    $query = $query->addSelect('usuario.email AS email_do_cliente');

    $query = $query->join('usuario', 'fk_usuario', '=', 'pk_usuario'); //INNER JOIN

    $query = $query->where('chamado.pk_chamado', '=', $pk_chamado);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = "Nenhum chamado com ID $pk_chamado foi encontrado no banco de dados";
      $mensagem_do_model .= ' do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $valores = (array) $array_resultado[0];

      $chamado = new Chamado();
      $chamado->set_pk_chamado($valores['pk_chamado']);
      $chamado->set_fk_usuario($valores['fk_usuario']);
      $chamado->set_titulo($valores['titulo']);
      $chamado->set_descricao($valores['descricao']);
      $chamado->set_anexos($valores['anexos']);
      $chamado->set_status($valores['status']);
      $chamado->set_momento_da_abertura($valores['momento_da_abertura']);
      $chamado->set_momento_do_inicio_do_atendimento($valores['momento_do_inicio_do_atendimento']);
      $chamado->set_momento_da_finalizacao($valores['momento_da_finalizacao']);

      $usuario = new Usuario();
      $usuario->set_pk_usuario($valores['pk_usuario']);
      $usuario->set_nome_completo($valores['nome_completo_do_cliente']);
      $usuario->set_email($valores['email_do_cliente']);

      $chamado->set_usuario($usuario);

      $array_melhorado[] = $chamado;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function selecionar_respostas($pk_chamado){
    $query = DB::table('resposta');
    $query = $query->addSelect('pk_resposta');
    $query = $query->addSelect('resposta.fk_usuario');
    $query = $query->addSelect('resposta.mensagem');
    $query = $query->addSelect('resposta.anexos');
    $query = $query->addSelect('resposta.momento_do_envio');
    $query = $query->addSelect('pk_usuario');
    $query = $query->addSelect('usuario.nome_completo AS nome_completo_do_autor');
    $query = $query->addSelect('usuario.email AS email_do_autor');

    $query = $query->join('usuario', 'fk_usuario', '=', 'pk_usuario'); //INNER JOIN

    $query = $query->where('resposta.fk_chamado', '=', $pk_chamado);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $valores = (array) $objeto_generico;

      $resposta = new Resposta();
      $resposta->set_pk_resposta($valores['pk_resposta']);
      $resposta->set_fk_usuario($valores['fk_usuario']);
      $resposta->set_mensagem($valores['mensagem']);
      $resposta->set_anexos($valores['anexos']);
      $resposta->set_momento_do_envio($valores['momento_do_envio']);

      $usuario = new Usuario();
      $usuario->set_pk_usuario($valores['pk_usuario']);
      $usuario->set_nome_completo($valores['nome_completo_do_autor']);
      $usuario->set_email($valores['email_do_autor']);

      $resposta->set_usuario($usuario);

      $array_melhorado[] = $resposta;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function registrar_resposta($resposta){
    $insert['fk_usuario'] = $resposta->get_fk_usuario();
    $insert['fk_chamado'] = $resposta->get_fk_chamado();
    $insert['mensagem'] = $resposta->get_mensagem();
    $insert['anexos'] = $resposta->get_anexos();
    $insert['momento_do_envio'] = $resposta->get_momento_do_envio();

    $array_resultado = array();

    try{
      DB::table('resposta')->insert($insert);
      $array_resultado['id_da_resposta'] = DB::getPdo()->lastInsertId();
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function atualizar_o_status_do_chamado_para_em_atendimento($pk_chamado, $momento_atual){
    $update['status'] = 'em_atendimento';
    $update['momento_do_inicio_do_atendimento'] = $momento_atual;

    $array_resultado = array();

    try{
      DB::table('chamado')->where('pk_chamado', '=', $pk_chamado)->update($update);
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function atualizar_o_status_do_chamado_para_finalizado($pk_chamado, $momento_atual){
    $update['status'] = 'finalizado';
    $update['momento_da_finalizacao'] = $momento_atual;

    $array_resultado = array();

    try{
      DB::table('chamado')->where('pk_chamado', '=', $pk_chamado)->update($update);
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
