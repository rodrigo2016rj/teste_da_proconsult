<?php

namespace App\Models\Entidades;

final class Usuario{
  private $pk_usuario;
  private $nome_completo;
  private $cpf;
  private $email;
  private $senha;
  private $momento_do_cadastro;
  private $tipo;

  public function __construct($array_usuario = array()){
    if(isset($array_usuario['pk_usuario'])){
      $this->pk_usuario = $array_usuario['pk_usuario'];
    }
    if(isset($array_usuario['nome_completo'])){
      $this->nome_completo = $array_usuario['nome_completo'];
    }
    if(isset($array_usuario['cpf'])){
      $this->cpf = $array_usuario['cpf'];
    }
    if(isset($array_usuario['email'])){
      $this->email = $array_usuario['email'];
    }
    if(isset($array_usuario['senha'])){
      $this->senha = $array_usuario['senha'];
    }
    if(isset($array_usuario['momento_do_cadastro'])){
      $this->momento_do_cadastro = $array_usuario['momento_do_cadastro'];
    }
    if(isset($array_usuario['tipo'])){
      $this->tipo = $array_usuario['tipo'];
    }
  }

  public function set_pk_usuario($pk_usuario){
    $this->pk_usuario = $pk_usuario;
  }

  public function set_nome_completo($nome_completo){
    $this->nome_completo = $nome_completo;
  }

  public function set_cpf($cpf){
    $this->cpf = $cpf;
  }

  public function set_email($email){
    $this->email = $email;
  }

  public function set_senha($senha){
    $this->senha = $senha;
  }

  public function set_momento_do_cadastro($momento_do_cadastro){
    $this->momento_do_cadastro = $momento_do_cadastro;
  }

  public function set_tipo($tipo){
    $this->tipo = $tipo;
  }

  public function get_pk_usuario(){
    return $this->pk_usuario;
  }

  public function get_nome_completo(){
    return $this->nome_completo;
  }

  public function get_cpf(){
    return $this->cpf;
  }

  public function get_email(){
    return $this->email;
  }

  public function get_senha(){
    return $this->senha;
  }

  public function get_momento_do_cadastro(){
    return $this->momento_do_cadastro;
  }

  public function get_tipo(){
    return $this->tipo;
  }

  public function enum_tipo(){
    $array_enum['cliente'] = 'Cliente';
    $array_enum['colaborador'] = 'Colaborador';
    return $array_enum;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'nome_completo':
        return 2;
      case 'cpf':
        return 14;
      case 'email':
        return 6;
      case 'senha':
        return 9;
    }
    return -1;
  }

  // O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados.
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'nome_completo':
        return 200;
      case 'cpf':
        return 14;
      case 'email':
        return 160;
      case 'senha':
        return 70;
    }
    return -1;
  }

  public function padrao_para_cpf(){
    return '/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/';
  }

}
