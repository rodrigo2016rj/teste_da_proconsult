<?php

namespace App\Models\Entidades;

final class Chamado{
  private $pk_chamado;
  private $fk_usuario;
  private $titulo;
  private $descricao;
  private $anexos;
  private $status;
  private $momento_da_abertura;
  private $momento_do_inicio_do_atendimento;
  private $momento_da_finalizacao;
  private $usuario;

  public function __construct($array_chamado = array()){
    if(isset($array_chamado['pk_chamado'])){
      $this->pk_chamado = $array_chamado['pk_chamado'];
    }
    if(isset($array_chamado['fk_usuario'])){
      $this->fk_usuario = $array_chamado['fk_usuario'];
    }
    if(isset($array_chamado['titulo'])){
      $this->titulo = $array_chamado['titulo'];
    }
    if(isset($array_chamado['descricao'])){
      $this->descricao = $array_chamado['descricao'];
    }
    if(isset($array_chamado['anexos'])){
      $this->anexos = $array_chamado['anexos'];
    }
    if(isset($array_chamado['status'])){
      $this->status = $array_chamado['status'];
    }
    if(isset($array_chamado['momento_da_abertura'])){
      $this->momento_da_abertura = $array_chamado['momento_da_abertura'];
    }
    if(isset($array_chamado['momento_do_inicio_do_atendimento'])){
      $this->momento_do_inicio_do_atendimento = $array_chamado['momento_do_inicio_do_atendimento'];
    }
    if(isset($array_chamado['momento_da_finalizacao'])){
      $this->momento_da_finalizacao = $array_chamado['momento_da_finalizacao'];
    }
    if(isset($array_chamado['usuario'])){
      $this->usuario = $array_chamado['usuario'];
    }
  }

  public function set_pk_chamado($pk_chamado){
    $this->pk_chamado = $pk_chamado;
  }

  public function set_fk_usuario($fk_usuario){
    $this->fk_usuario = $fk_usuario;
  }

  public function set_titulo($titulo){
    $this->titulo = $titulo;
  }

  public function set_descricao($descricao){
    $this->descricao = $descricao;
  }

  public function set_anexos($anexos){
    $this->anexos = $anexos;
  }

  public function set_status($status){
    $this->status = $status;
  }

  public function set_momento_da_abertura($momento_da_abertura){
    $this->momento_da_abertura = $momento_da_abertura;
  }

  public function set_momento_do_inicio_do_atendimento($momento_do_inicio_do_atendimento){
    $this->momento_do_inicio_do_atendimento = $momento_do_inicio_do_atendimento;
  }

  public function set_momento_da_finalizacao($momento_da_finalizacao){
    $this->momento_da_finalizacao = $momento_da_finalizacao;
  }

  public function set_usuario($usuario){
    $this->usuario = $usuario;
  }

  public function get_pk_chamado(){
    return $this->pk_chamado;
  }

  public function get_fk_usuario(){
    return $this->fk_usuario;
  }

  public function get_titulo(){
    return $this->titulo;
  }

  public function get_descricao(){
    return $this->descricao;
  }

  public function get_anexos(){
    return $this->anexos;
  }

  public function get_status(){
    return $this->status;
  }

  public function get_momento_da_abertura(){
    return $this->momento_da_abertura;
  }

  public function get_momento_do_inicio_do_atendimento(){
    return $this->momento_do_inicio_do_atendimento;
  }

  public function get_momento_da_finalizacao(){
    return $this->momento_da_finalizacao;
  }

  public function get_usuario(){
    return $this->usuario;
  }

  public function enum_status(){
    $array_enum['aberto'] = 'Aberto';
    $array_enum['em_atendimento'] = 'Em atendimento';
    $array_enum['finalizado'] = 'Finalizado';
    return $array_enum;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'titulo':
        return 10;
      case 'descricao':
        return 30;
    }
    return -1;
  }

  // O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados.
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'titulo':
        return 60;
      case 'descricao':
        return 2000;
      case 'anexos':
        return 1000;
    }
    return -1;
  }

}
