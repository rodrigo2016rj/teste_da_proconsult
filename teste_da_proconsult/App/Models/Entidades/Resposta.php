<?php

namespace App\Models\Entidades;

final class Resposta{
  private $pk_resposta;
  private $fk_usuario;
  private $fk_chamado;
  private $mensagem;
  private $anexos;
  private $momento_do_envio;
  private $usuario;
  private $chamado;

  public function __construct($array_resposta = array()){
    if(isset($array_resposta['pk_resposta'])){
      $this->pk_resposta = $array_resposta['pk_resposta'];
    }
    if(isset($array_resposta['fk_usuario'])){
      $this->fk_usuario = $array_resposta['fk_usuario'];
    }
    if(isset($array_resposta['fk_chamado'])){
      $this->fk_chamado = $array_resposta['fk_chamado'];
    }
    if(isset($array_resposta['mensagem'])){
      $this->mensagem = $array_resposta['mensagem'];
    }
    if(isset($array_resposta['anexos'])){
      $this->anexos = $array_resposta['anexos'];
    }
    if(isset($array_resposta['momento_do_envio'])){
      $this->momento_do_envio = $array_resposta['momento_do_envio'];
    }
    if(isset($array_resposta['usuario'])){
      $this->usuario = $array_resposta['usuario'];
    }
    if(isset($array_resposta['chamado'])){
      $this->chamado = $array_resposta['chamado'];
    }
  }

  public function set_pk_resposta($pk_resposta){
    $this->pk_resposta = $pk_resposta;
  }

  public function set_fk_usuario($fk_usuario){
    $this->fk_usuario = $fk_usuario;
  }

  public function set_fk_chamado($fk_chamado){
    $this->fk_chamado = $fk_chamado;
  }

  public function set_mensagem($mensagem){
    $this->mensagem = $mensagem;
  }

  public function set_anexos($anexos){
    $this->anexos = $anexos;
  }

  public function set_momento_do_envio($momento_do_envio){
    $this->momento_do_envio = $momento_do_envio;
  }

  public function set_usuario($usuario){
    $this->usuario = $usuario;
  }

  public function set_chamado($chamado){
    $this->chamado = $chamado;
  }

  public function get_pk_resposta(){
    return $this->pk_resposta;
  }

  public function get_fk_usuario(){
    return $this->fk_usuario;
  }

  public function get_fk_chamado(){
    return $this->fk_chamado;
  }

  public function get_mensagem(){
    return $this->mensagem;
  }

  public function get_anexos(){
    return $this->anexos;
  }

  public function get_momento_do_envio(){
    return $this->momento_do_envio;
  }

  public function get_usuario(){
    return $this->usuario;
  }

  public function get_chamado(){
    return $this->chamado;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'mensagem':
        return 30;
    }
    return -1;
  }

  // O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados.
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'mensagem':
        return 2000;
      case 'anexos':
        return 1000;
    }
    return -1;
  }

}
